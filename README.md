[Andrew Chumchal]
============

My personal website. Introduces me. 

Built with [DocPad], [Markdown], [Bootstrap], and [Node.js]. Hosted on [GitHub Pages].

![Build Status](https://travis-ci.org/andrewchumchal/AndrewChumchal.com.svg?branch=master)

Build instructions
------------------ 

1. Download and install node.js from http://www.nodejs.org/
2. Install DocPad: https://docpad.bevry.me/start/install
3. Install ImageMagic: http://www.imagemagick.org/script/binary-releases.php (or `brew install imagemagick` on OS X w/ homebrew)
4. Recompile plugins (if necessary): `npm rebuild`
5. Run `docpad run` in this directory to compile and view the site locally
6. Edit or remove `src/files/CNAME` and then run `docpad deploy-ghpages` to deploy to GitHub









[Andrew Chumchal]: http://gh.andrewchumchal.com.com/ "Andrew Chumchal"
[Node.js]: http://www.nodejs.org/
[DocPad]: http://docpad.org/
[Markdown]: http://daringfireball.net/projects/markdown/
[Bootstrap]: http://getbootstrap.com/
[GitHub Pages]: http://pages.github.com/
[Travis CI]: https://travis-ci.org/
[Amazon CloudFront]: https://aws.amazon.com/cloudfront/
